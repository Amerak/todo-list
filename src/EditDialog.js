import React from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from '@material-ui/core/Grid';
import {green} from '@material-ui/core/colors';
import {
    ThemeProvider,
    createMuiTheme,
} from '@material-ui/core/styles';
import Switch from '@material-ui/core/Switch';

export default class NewItem extends React.Component {
    constructor() {
        super();
        this.handleChangeCount = this.handleChangeCount.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeText = this.handleChangeText.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.state = {
            countError: false,
            textError: false
        }
    }

    async componentDidMount() {
        this.setState({
            ...this.state,
            text: this.props.item.text,
            count: this.props.item.count,
            completed: this.props.item.completed
        })
    }

    handleChangeText(opt) {
        if (opt.target.value) {
            this.setState({
                ...this.state,
                text: opt.target.value,
                textError: false
            });
        } else {
            this.setState({
                ...this.state,
                text: "",
                textError: true
            });
        }
    }

    handleChangeCount(opt) {
        if (opt.target.value < 0.1) {
            this.setState({
                ...this.state,
                countError: true
            });
        } else if (!isNaN(opt.target.value)) {
            this.setState({
                ...this.state,
                count: opt.target.value,
                countError: false
            });
        }
    }

    handleSubmit(event) {
        event.preventDefault();
        if (typeof this.props.onItemEdit === "function") {
            this.props.onItemEdit({
                ...this.props.item,
                text: this.state.text,
                count: this.state.count,
                completed: this.state.completed
            });
        }
        this.handleClose();
    }

    handleClose() {
        this.props.callback()
    }

    render() {
        let theme = createMuiTheme({
            palette: {
                primary: green,
            },
            overrides: {
                MuiButton: {
                    root: {
                        background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
                        "&$disabled": {
                            background: "white"
                        }
                    },
                }
            }
        });

        return (
            <Dialog open={this.props.open}>
                <DialogTitle id="title">Edit item {this.props.item.text}</DialogTitle>
                <DialogContent>
                    <form noValidate autoComplete="off">
                        <ThemeProvider theme={theme}>
                            <Switch
                                checked={this.state.completed}
                                onChange={() => {
                                    this.setState({
                                        ...this.state,
                                        completed: !this.state.completed
                                    })
                                }}
                            /> {this.state.completed ? "Completed" : "Not completed"}
                            <Grid container direction={"row"}>
                                <Grid item xl={3} s={12}>
                                    <TextField error={this.state.countError} defaultValue={this.props.item.count}
                                               id="Count" label="Count"
                                               variant="filled" type={"number"}
                                               onChange={this.handleChangeCount}/>
                                </Grid>
                                <Grid item xl={9} s={12}>
                                    <TextField required error={this.state.textError} id="Text" label="Text"
                                               defaultValue={this.props.item.text}
                                               variant="filled"
                                               onChange={this.handleChangeText}/>
                                </Grid> </Grid>
                        </ThemeProvider>
                    </form>
                </DialogContent>

                <DialogActions>
                    <ThemeProvider theme={theme}>
                        <Button variant="contained" size="medium"
                                disabled={(this.state.countError || this.state.text === "")}
                                onClick={this.handleSubmit}>Edit</Button>
                    </ThemeProvider>

                    <Button variant="contained" size="medium" color="primary" onClick={this.handleClose}>Close</Button>
                </DialogActions>
            </Dialog>
        );
    }
}