import React from 'react';
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Checkbox from "@material-ui/core/Checkbox";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/DeleteForever';
import EditIcon from '@material-ui/icons/Edit';
import Grid from "@material-ui/core/Grid";
import EditDialog from "./EditDialog";
import Tooltip from '@material-ui/core/Tooltip';

export default class TodoItem extends React.Component {
    constructor() {
        super();
        this.handleDelete = this.handleDelete.bind(this);
        this.handleToggle = this.handleToggle.bind(this);
        this.createDialog = this.createDialog.bind(this);
        this.editItemCallback = this.editItemCallback.bind(this);
        this.state = {
            editDialog: false
        }
    }


    handleDelete() {
        if (typeof this.props.onDelete === "function") {
            this.props.onDelete(this.props.item.id);
        }
    }

    handleToggle() {
        if (typeof this.props.onToggle === "function") {
            this.props.onToggle(this.props.item.id);
        }
    }

    createDialog() {
        return <EditDialog open={this.state.editDialog} onItemEdit={this.props.onEdit} callback={this.editItemCallback}
                           item={this.props.item}/>
    }

    editItemCallback() {
        this.setState({...this.state, editDialog: false})
    }

    render() {
        return (
            <Grid item xs={12}>
                {this.state.editDialog ? this.createDialog() : ""}
                <ListItem key={this.props.id} dense button onClick={this.handleToggle}>
                    <ListItemIcon>
                        <Checkbox
                            edge="start"
                            checked={this.props.item.completed}
                            disableRipple
                            onChange={this.handleToggle}
                        />
                    </ListItemIcon>
                    <ListItemText id={this.props.item.id} primary={this.props.item.text}
                                  secondary={this.props.item.count + " pcs"}/>

                    <ListItemSecondaryAction>
                        <Tooltip title={"Edit item"}>
                            <IconButton color="primary" edge="end" aria-label="Edit" onClick={() => {
                                this.setState({...this.state, editDialog: true})
                            }}>
                                <EditIcon/>
                            </IconButton>
                        </Tooltip>
                        <Tooltip title={"Delete item"}>
                            <IconButton color="secondary" edge="end" aria-label="Delete" onClick={this.handleDelete}>
                                <DeleteIcon/>
                            </IconButton>
                        </Tooltip>
                    </ListItemSecondaryAction>
                </ListItem>
            </Grid>
        )
    }
}