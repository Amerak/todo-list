import React from 'react';
import NewItem from './NewItem';
import TodoItem from './TodoItem';
import Loading from "./Loading";
import Calls from "./calls";

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Fab from '@material-ui/core/Fab';
import List from '@material-ui/core/List';
import AddIcon from '@material-ui/icons/Add';
import Tooltip from '@material-ui/core/Tooltip';

export default class TodoList extends React.Component {
    constructor() {
        super();
        this.handleItemCreate = this.handleItemCreate.bind(this);
        this.renderItems = this.renderItems.bind(this);
        this.createItemCallback = this.createItemCallback.bind(this);
        this.createDialog = this.createDialog.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleToggle = this.handleToggle.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.state = {
            openDialog: false,
            items: [],
            loadingState: "INIT"
        };
    }

    async componentDidMount() {
        this.setState({loadingState: "LOADING"});
        try {
            let result = await Calls.getShoppingList();
            this.setState({items: result, loadingState: "DONE"});
        } catch (error) {
            this.setState({error: error, loadingState: "ERROR"});
        }
    }

    async handleItemCreate(item, count) {
        let serverItem = await Calls.createShoppingItem({
            text: item,
            count: count,
            completed: false,
            createdAt: new Date().toUTCString()
        });
        this.setState({
            items: [...this.state.items, serverItem]
        });
    }

    async handleDelete(itemId) {
        await Calls.deleteShoppingItem({id: itemId});
        let newArray = this.state.items.filter((value) => value.id !== itemId);
        this.setState({
            items: newArray
        })
    }

    async handleToggle(itemId) {
        let newArray = this.state.items.map((value) => value.id !== itemId ? value : {
            text: value.text,
            count: value.count,
            completed: !value.completed,
            createdAt: value.createdAt,
            id: value.id
        });
        await this.setState({
            items: newArray
        });
        let item = this.state.items.find((value) => value.id === itemId);
        await Calls.updateShoppingItem(item);
    }

    async handleEdit(newItem) {
        let newArray = this.state.items.map((value) => value.id !== newItem.id ? value : newItem);
        await this.setState({
            items: newArray
        });
        await Calls.updateShoppingItem(newItem);
    }

    renderItems() {
        return this.state.items.map((value) => <TodoItem key={value.id} item={value} onDelete={this.handleDelete}
                                                         onEdit={this.handleEdit}
                                                         onToggle={this.handleToggle}/>)
    }

    createDialog() {
        return <NewItem open={this.state.openDialog} onItemCreate={this.handleItemCreate}
                        callback={this.createItemCallback}/>
    }

    createItemCallback() {
        this.setState({...this.state, openDialog: false})
    }

    render() {

        let theme = createMuiTheme({
            overrides: {
                MuiAppBar: {
                    root: {
                        background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
                        borderRadius: 3,
                        border: 0,
                        color: 'white',
                        height: 48,
                        padding: '0 30px',
                        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
                    },
                },
                MuiFab: {
                    root: {
                        background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
                        color: "white",
                        position: "fixed",
                        bottom: '5%',
                        right: '5%'
                    }
                }
            },
        });

        switch (this.state.loadingState) {
            case "LOADING":
                return <Loading/>;
            case "DONE":
                return (
                    <ThemeProvider theme={theme}>
                        <div>
                            <AppBar position={"relative"}>
                                <Grid item xl={12}>
                                    <Toolbar variant="dense">
                                        <Typography variant="h6" color="inherit">
                                            Another TODO-LIST
                                        </Typography>
                                    </Toolbar>
                                </Grid>
                            </AppBar>
                        </div>
                        <List>
                            <Grid container spacing={2}>
                                {this.renderItems()}
                            </Grid>
                        </List>
                        {this.state.openDialog ? this.createDialog() : ""}
                        <Tooltip title="Create item">
                            <Fab aria-label={'add'} onClick={() => {
                                this.setState({...this.state, openDialog: true})
                            }}>
                                <AddIcon/>
                            </Fab>
                        </Tooltip>
                    </ThemeProvider>
                );

            case "ERROR":
                return <div>Error: {"" + this.state.error}</div>;
            default:
                return <div>Error</div>;
        }
    }
}